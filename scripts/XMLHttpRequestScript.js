var xhr = new XMLHttpRequest();
//var url = 'https://jsonplaceholder.typicode.com/users/2/posts';

var url = 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/A_Meat_Stall_with_the_Holy_Family_Giving_Alms_-_Pieter_Aertsen_-_Google_Cultural_Institute.jpg/1280px-A_Meat_Stall_with_the_Holy_Family_Giving_Alms_-_Pieter_Aertsen_-_Google_Cultural_Institute.jpg';
xhr.open('GET', url);
//xhr.setRequestHeader('Access-Control-Allow-Origin','<origin>');
xhr.send();

xhr.onload = function() {
	console.log('Request completed');
}

xhr.onerror = function() {
	console.log('Error');
}
xhr.onprogress = function(event) {
	console.log('event.loaded: '+ event.loaded);
	console.log('event.total: ' + event.total);
	var percent = Math.floor(event.loaded/event.total * 100);
	console.log('percent: ' + percent);
}
